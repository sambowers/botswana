import biota
import os

# Processing parameters
years = [2015, 2016]
forest_thresholds = [5, 10]

# Data locations
data_dir = '~/SMFM/ALOS_data/'
output_pattern = '~/SMFM/botswana/DATA_%stC/%s/'

# For each year and forest_threshold...
for year in years:
    for forest_threshold in forest_thresholds:
        
        # Set output_dir
        output_dir = output_pattern%(str(forest_threshold),str(year))
        
        # For lat/lon range (southern Africa)
        for lat in range(-35, -14):
            for lon in range(11, 42):
                
                # Load ALOS mosaic tile, skip if nonexistent
                try:
                    tile = biota.LoadTile(data_dir, lat, lon, year, forest_threshold = forest_threshold, area_threshold = 0.5, lee_filter = True, output_dir = output_dir)
                    print('Doing lat: %s, lon: %s, year: %s, forest_threshold: %s.'%(str(lat), str(lon), str(year), str(forest_threshold)))

                except:
                    continue
                
                # Calibrate to woody cover and output to GeoTIff
                tile.getWoodyCover(output = True)
         
        # Mosaic
        os.system('gdal_merge.py %sWoodyCover_%s_???????.tif -co "COMPRESS=LZW" -o %s/WoodyCover_%s_%stC.tif'%(output_dir, str(year), output_dir, str(year), str(forest_threshold)))
