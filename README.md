# Prototype forest cover map for ~~Botswana~~ Southern Africa.

Data processed with biota (https://www.bitbucket.org/sambowers/biota)

Contact: sam.bowers@ed.ac.uk

Note the following assumptions:

* Radar backscatter is more closely related to biomass than to canopy cover, so in place of a 10 % canopy cover threshold I've tested thresholds of 5 and 10 tC/ha. If used operationally, this threshold would have to be tuned to match local understanding, or ideally calibrated with field plots.
* This uses a 0.5 ha minimum forest area, calculated by a minimum number of connected pixels (diagonals included).
* I've not masked out urban or flooded vegetation, which tend to get included as forest.
* As well as vegetation properties, radar backscatter is sensitive to soil moisture, especially at the low biomass values such as in Botswana. There may be some soil moisture artefacts in the form of 'striping', where two adjacent overpasses were acquired at different times of year.

![picture](SMFM_2015.png)
